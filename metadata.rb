name             'ecmc_mysql'
maintainer       'University of Edinburgh'
maintainer_email 'paul.d.mitchell.ed.ac.uk'
license          'All rights reserved'
description      'Installs/Configures MySQL in docker'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.3.1'

depends 'apt'
depends 'docker'
depends 'hostsfile'

supports 'ubuntu'

require 'serverspec'

set :backend, :exec

describe host('mysql') do
  it { should be_resolvable.by('hosts')}
end

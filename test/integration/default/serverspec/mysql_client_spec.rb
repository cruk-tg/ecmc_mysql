require 'serverspec'

set :backend, :exec

describe command("/usr/bin/mysql --version") do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match /5\.6\.\d+/ }
end

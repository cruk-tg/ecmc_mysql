describe docker_image('mysql:5.7') do
  it { should exist }
end

describe docker_container('mysql_5_7') do
  it { should be_running }
  its('ports') { should eq '0.0.0.0:3306->3306/tcp'}
end

#
# Cookbook Name:: ecmc_mysql_5_7
# Recipe:: default
#
# Copyright 2016, The University of Edinburgh
#
# All rights reserved - Do Not Redistribute
#
docker_image 'mysql' do
  tag '5.7'
  action :pull
end

docker_volume 'mysql_5_7_data'

docker_container 'mysql_5_7' do
  repo 'mysql'
  tag '5.7'
  env "MYSQL_ROOT_PASSWORD=#{node['mysql']['mysql_root_password']}"
  volumes ['mysql_5_7_data:/var/lib/mysql']

  port '3306:3306'
  restart_policy 'always'
  action :run
end

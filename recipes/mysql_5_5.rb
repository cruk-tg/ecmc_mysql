#
# Cookbook Name:: ecmc_mysql_5_5
# Recipe:: default
#
# Copyright 2016, The University of Edinburgh
#
# All rights reserved - Do Not Redistribute
#
docker_image 'mysql' do
  tag '5.5'
  action :pull
end

docker_volume 'mysql_5_5_data'

docker_container 'mysql_5_5' do
  repo 'mysql'
  tag '5.5'
  env "MYSQL_ROOT_PASSWORD=#{node['mysql']['mysql_root_password']}"
  volumes ['mysql_5_5_data:/var/lib/mysql']
  port '3306:3306'
  restart_policy 'always'
end

#
# Cookbook Name:: ecmc_mysql
# Recipe:: default
#
# Copyright 2016, University of Edinburgh
#
# All rights reserved - Do Not Redistribute
#
include_recipe "apt"

package "mysql-client"

hostsfile_entry '127.0.0.1' do
  hostname 'mysql'
  action :append
end
